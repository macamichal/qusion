package cz.maca.qusion.view.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import cz.maca.qusion.Constants
import cz.maca.qusion.MyApplication
import cz.maca.qusion.R
import cz.maca.qusion.model.Employee
import cz.maca.qusion.viewModel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import cz.maca.qusion.di.Result
import cz.maca.qusion.view.adapter.EmployeeAdapter
import cz.maca.qusion.view.show

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: ListViewModel

    private val employeeAdapter = EmployeeAdapter(arrayListOf()) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(Constants().EMP, it)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as MyApplication).appComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = employeeAdapter
        }

        mainViewModel = ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java]

        fab.setOnClickListener {
            mainViewModel.loadData()
        }

        subscribeUi()

        if (savedInstanceState == null) {
            mainViewModel.loadData()
        }
    }

    private fun subscribeUi() {
        mainViewModel.employeesState.observe(this, Observer<Result<MutableList<Employee>>> { result ->

            Log.d("result", result.toString())

            when (result) {
                Result.Loading -> {
                    showProgress(true)
                }
                is Result.Success<MutableList<Employee>> -> {
                    showProgress(false)
                    Log.d("DATA", "count: ${result.data.size}")
                    employeeAdapter.addEmployees(result.data)
                }
                is Result.Error -> {
                    showProgress(false)
                    Log.d("ERRR", result.exception.toString())
                    Toast.makeText(this, getText(R.string.error_load), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun showProgress(value: Boolean) {
        progress.show(value)
        list.show(!value)
    }

}
