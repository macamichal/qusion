package cz.maca.qusion.view

import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView

fun ProgressBar.show(value: Boolean) {
    visibility = if(value) View.VISIBLE else View.GONE
}
fun RecyclerView.show(value: Boolean) {
    visibility = if(value) View.VISIBLE else View.GONE
}
