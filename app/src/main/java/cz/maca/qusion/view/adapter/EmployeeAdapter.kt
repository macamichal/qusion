package cz.maca.qusion.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.maca.qusion.R
import cz.maca.qusion.model.Employee
import kotlinx.android.synthetic.main.list_item_employee.view.*

class EmployeeAdapter(var dataSet: MutableList<Employee>, val listener: (Employee) -> Unit) :
    RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EmployeeViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_employee, parent, false)
    )

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        holder.bind(dataSet[position], listener)
    }

    fun addEmployees(employees: MutableList<Employee>) {
        dataSet = employees
        notifyDataSetChanged()
    }

    class EmployeeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.text_view_name
        private val age = view.text_view_age
        private val card = view.cardItem

        fun bind(employee: Employee, listener: (Employee) -> Unit) = with(itemView) {
            name.text = employee.name
            age.text = employee.age.toString()
            card.setOnClickListener {
                listener(employee)
            }
        }
    }

    override fun getItemCount(): Int = dataSet.size
}