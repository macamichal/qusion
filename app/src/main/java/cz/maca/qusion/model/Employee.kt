package cz.maca.qusion.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Employee(
    val id: Int?,
    @SerializedName("employee_name") val name: String?,
    @SerializedName("employee_salary") val salary: Int?,
    @SerializedName("employee_age") val age: Int?
) : Serializable
