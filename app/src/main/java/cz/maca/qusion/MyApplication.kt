package cz.maca.qusion

import android.app.Application
import cz.maca.qusion.di.DaggerApplicationComponent

class MyApplication : Application() {
    val appComponent = DaggerApplicationComponent.create()
}