package cz.maca.qusion.di

import dagger.Module

@Module(includes = [ViewModelModule::class])
class AppModule