package cz.maca.qusion.di

import cz.maca.qusion.view.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class]
)
interface ApplicationComponent {
    fun inject(mainActivity: MainActivity)
}