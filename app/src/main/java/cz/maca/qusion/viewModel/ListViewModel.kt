package cz.maca.qusion.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.maca.qusion.model.Employee
import cz.maca.qusion.repository.EmployeeRepository
import kotlinx.coroutines.launch
import javax.inject.Inject
import cz.maca.qusion.di.Result

class ListViewModel @Inject constructor(private val employeeRepository: EmployeeRepository) : ViewModel() {
    private val _employeesState = MutableLiveData<Result<MutableList<Employee>>>()
    val employeesState: LiveData<Result<MutableList<Employee>>> = _employeesState

    fun loadData() {
        _employeesState.value = Result.Loading

        viewModelScope.launch {
            val tasksResult = employeeRepository.loadData()

            _employeesState.value = tasksResult
        }
    }
}