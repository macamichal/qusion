package cz.maca.qusion.repository.retrofit.api

import cz.maca.qusion.model.Employee
import retrofit2.Call
import retrofit2.http.GET

interface DataApi {

    @GET("/api/v1/employees")
    fun getEmployees(): Call<List<Employee>>
}