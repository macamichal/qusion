package cz.maca.qusion.repository

import cz.maca.qusion.model.Employee
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import cz.maca.qusion.di.Result
import cz.maca.qusion.repository.retrofit.Retrofit
import cz.maca.qusion.repository.retrofit.api.DataApi
import javax.inject.Inject

class EmployeeRepository @Inject constructor() {

    private val dataApi: DataApi = Retrofit().createClient(DataApi::class.java, "http://dummy.restapiexample.com")

    suspend fun loadData(): Result<MutableList<Employee>> {

        return withContext(Dispatchers.IO) {

            val response: Response<List<Employee>> =
                dataApi.getEmployees().execute() ?: return@withContext Result.Error(Exception("Illegal state"))

            return@withContext if (response.isSuccessful) {
                Result.Success(response.body().toMutableList())
            } else {
                Result.Error(Exception("${response.code()}: ${response.message()}"))
            }
        }

    }
}